export const PROFILE_OPTIONS = [
  'profile',
  'closeSession'
];

export const PROFILE_IMAGE_STYLES = {
  borderRadius: '50%',
  height: '35px',
  width: '35px',
  cursor: 'pointer'
}