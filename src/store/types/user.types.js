export const SET_TOKEN = 'SET_TOKEN';
export const CLEAN_TOKEN = 'CLEAN_TOKEN';

export const SET_USER_INFO = 'SET_USER_INFO';
export const CLEAN_USER_INFO = 'CLEAN_USER_INFO';