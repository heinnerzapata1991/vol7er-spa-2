# Specify a base image
FROM mhart/alpine-node:10

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

RUN npm install -g yarn

RUN apk add --no-cache --virtual .gyp \
        python \
        make \
        g++
